﻿using System;
using System.Linq;
using AutoFixture;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.UnitTests.Creator;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests : TestsBase
    {
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_RequestLimitIsNotPositive_ReturnsBadRequest()
        {
            // Arrange
            var request = Fixture
                .Create<SetPartnerPromoCodeLimitRequest>()
                .WithLimit(0);

            // Act
            var result = await PartnersController.SetPartnerPromoCodeLimitAsync(It.IsAny<Guid>(), request);
 
            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var request = Fixture.Create<SetPartnerPromoCodeLimitRequest>();

            PartnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync((Partner)null);
            
            // Act
            var result = await PartnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
 
            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = Creator.Creator
                .CreateBasePartner()
                .WithActiveStatus(false);
            
            var request = Fixture.Create<SetPartnerPromoCodeLimitRequest>();
            
            PartnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await PartnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
 
            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_RequestIsCorrect_ReturnsCreatedAtActionResult()
        {
            // Arrange
            var partner = Creator.Creator
                .CreateBasePartner()
                .WithActiveStatus(true);
            
            var request = Fixture
                .Create<SetPartnerPromoCodeLimitRequest>()
                .WithLimit(1);
            
            PartnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await PartnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);
 
            // Assert
            PartnersRepositoryMock.Verify(x=> x.GetByIdAsync(partner.Id), Times.Once);
            PartnersRepositoryMock.Verify(x=> x.UpdateAsync(It.IsAny<Partner>()), Times.Once);
            result.Should().BeAssignableTo<CreatedAtActionResult>();
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerHasActiveLimit_NumberIssuedPromoCodesIsReset()
        {
            // Arrange
            var partner = Creator.Creator
                .CreateBasePartner()
                .WithActiveStatus(true)
                .WithActiveLimit(100)
                .WithNumberIssuedPromoCodes(50);
            
            var request = Fixture.Create<SetPartnerPromoCodeLimitRequest>();
            
            PartnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await PartnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);
            
            // Assert
            PartnersRepositoryMock.Verify(x=> x.GetByIdAsync(partner.Id), Times.Once);
            PartnersRepositoryMock.Verify(x=> x.UpdateAsync(partner), Times.Once);
            partner.NumberIssuedPromoCodes.Should().Be(0);
            result.Should().BeAssignableTo<CreatedAtActionResult>();
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerHasInactiveLimit_NumberIssuedPromoCodesIsNotReset()
        {
            // Arrange
            const int numberOfIssuedOfPromoCodes = 50;
            
            var partner = Creator.Creator
                .CreateBasePartner()
                .WithActiveStatus(true)
                .WithInactiveLimit(100)
                .WithNumberIssuedPromoCodes(numberOfIssuedOfPromoCodes);
            
            var request = Fixture.Create<SetPartnerPromoCodeLimitRequest>();
            
            PartnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await PartnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);
            
            // Assert
            PartnersRepositoryMock.Verify(x=> x.GetByIdAsync(partner.Id), Times.Once);
            PartnersRepositoryMock.Verify(x=> x.UpdateAsync(partner), Times.Once);
            partner.NumberIssuedPromoCodes.Should().Be(numberOfIssuedOfPromoCodes);
            result.Should().BeAssignableTo<CreatedAtActionResult>();
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerHasActiveLimit_ExistingLimitIsCancelled()
        {
            // Arrange
            var partner = Creator.Creator
                .CreateBasePartner()
                .WithActiveStatus(true)
                .WithActiveLimit(100);

            var existingLimit = partner.PartnerLimits.Single();
            
            var request = Fixture.Create<SetPartnerPromoCodeLimitRequest>();
            
            PartnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await PartnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);
            
            // Assert
            PartnersRepositoryMock.Verify(x=> x.GetByIdAsync(partner.Id), Times.Once);
            PartnersRepositoryMock.Verify(x=> x.UpdateAsync(partner), Times.Once);
            existingLimit.CancelDate.Should().NotBeNull();
            result.Should().BeAssignableTo<CreatedAtActionResult>();
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_CorrectLimitSpecified_LimitIsSaved()
        {
            // Arrange
            var partner = Creator.Creator
                .CreateBasePartner()
                .WithActiveStatus(true);

            var request = Fixture.Create<SetPartnerPromoCodeLimitRequest>();
            
            PartnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            await PartnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);
            
            // Assert
            PartnersRepositoryMock.Verify(x=> x.GetByIdAsync(partner.Id), Times.Once);
            PartnersRepositoryMock.Verify(x=> x.UpdateAsync(partner), Times.Once);

            var updatedPartner = await PartnersRepositoryMock.Object.GetByIdAsync(partner.Id);
            updatedPartner.PartnerLimits.Should().HaveCount(1);
        }
    }
}