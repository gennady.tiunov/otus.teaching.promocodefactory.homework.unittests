﻿using AutoFixture;
using AutoFixture.AutoMoq;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class TestsBase
    {
        protected readonly IFixture Fixture = new Fixture().Customize(new AutoMoqCustomization());
        
        protected readonly Mock<IRepository<Partner>> PartnersRepositoryMock;
        protected readonly PartnersController PartnersController;

        protected TestsBase()
        {
            PartnersRepositoryMock = Fixture.Freeze<Mock<IRepository<Partner>>>();
            PartnersController = Fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }
    }
}