﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Creator
{
    public static class Creator
    {
        public static Partner CreateBasePartner()
        {
            var partner = new Partner
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                PartnerLimits = new List<PartnerPromoCodeLimit>()
            };

            return partner;
        }

        public static Partner WithActiveStatus(
            this Partner partner,
            bool isActive)
        {
            partner.IsActive = isActive;
            return partner;
        }
        
        public static Partner WithNumberIssuedPromoCodes(
            this Partner partner,
            int numberIssuedPromoCodes)
        {
            partner.NumberIssuedPromoCodes = numberIssuedPromoCodes; 
            return partner;
        }
        
        public static Partner WithActiveLimit(
            this Partner partner,
            int limit)
        {
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimit
                {
                    Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                    CreateDate = new DateTime(2020, 01, 01),
                    EndDate = DateTime.Now.AddDays(1),
                    CancelDate = null,
                    Limit = limit
                }
            };
            return partner;
        }
        
        public static Partner WithInactiveLimit(
            this Partner partner,
            int limit)
        {
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimit
                {
                    Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                    CreateDate = new DateTime(2020, 01, 01),
                    EndDate = DateTime.Now.AddDays(1),
                    CancelDate = DateTime.Now.AddDays(-1),
                    Limit = limit
                }
            };
            return partner;
        }
        
        public static SetPartnerPromoCodeLimitRequest WithLimit(
            this SetPartnerPromoCodeLimitRequest request,
            int limit)
        {
            request.Limit = limit;
            return request;
        }
    }
}